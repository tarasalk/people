<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserRoles extends Model {
	protected $guarded = ['id'];

	public function role() {
		return $this->hasOne('App\Models\Roles', 'id', 'role_id');
	}
}
