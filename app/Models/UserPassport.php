<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserPassport extends Model
{
    protected $table = 'user_passport';
	protected $guarded = [];
}
