<?php

namespace App\Models;

use App\Facades\CenomarketApi;
use App\Facades\PartnerApi;
use App\Facades\Sms;

use Illuminate\Database\Eloquent\Model;

class P2PTransfer extends Model
{
    /**
     * status field
     */
    const IN_PROGRESS = 'IN_PROGRESS';
    const CONFIRMED = 'CONFIRMED';
    const CONFIRMATION_CODE_ATTEMPTS_OVERFLOW = 'CONFIRMATION_CODE_ATTEMPTS_OVERFLOW';
    const SUCCESS = 'SUCCESS';
    const CANCELLED = 'CANCELLED';
    const NOT_ENOUGH_PROFIT = 'NOT_ENOUGH_PROFIT';
    const PROTECTION_CODE_ATTEMPTS_OVERFLOW = 'PROTECTION_CODE_ATTEMPTS_OVERFLOW';
    const EXPIRED = 'EXPIRED';  // TODO: перенести установку в задачу очереди
    const UNKNOWN_ERROR = 'UNKNOWN_ERROR';

    const OPERATION_PROTECTION_CODE_ATTEMPTS_MAX = 5;
    const OPERATION_CONFIRMATION_CODE_ATTEMPTS_MAX = 5;
    const OPERATION_TIMEOUT_SEC = 300;

    protected $guarded = ['id'];
    protected $table = 'p2p_transfers';

    /**
     * Запрос перевода
     * @param $from
     * @param $to
     * @param $amount
     * @return array
     */
    static public function request($from, $to, $amount) {
        // TODO: заменить на получение из текущей бд
        $profit = CenomarketApi::getUserProfit($from);

        if ($profit < $amount) {
            return ['success' => false, 'error' => 'NOT_ENOUGH_PROFIT'];
        }

        $protection_code = rand(1000, 9999);
        $confirmation_code = rand(1000, 9999);
        $operation_key = sha1(mt_rand() . time() . $protection_code);

        parent::create([
            'from' => $from,
            'to' => $to,
            'amount' => $amount,
            'operation_key' => $operation_key,
            'code' => $protection_code,
            'confirmation_code' => $confirmation_code
        ]);

        Sms::SendSmsByUserId($from, 'Код потверждения перевода: ' . $confirmation_code . '. Вы переводите ' . ($amount / 1e4) . ' руб.');

        return ['success' => true, 'data' => ['operation_key' => $operation_key]];
    }

    /**
     * Подтверждение запроса отправителем смс-кодом
     * @param $from
     * @param $operation_key
     * @param $confirmation_code
     * @return array
     */
    static public function confirmRequest($from, $operation_key, $confirmation_code) {
        $transfer = P2PTransfer::where([
            'from' => $from,
            'operation_key' => $operation_key
        ])->first();

        if (is_null($transfer)) {
            return ['success' => false, 'error' => 'OPERATION_NOT_FOUND'];
        }

        $transfer->confirmation_attempts_count += 1;
        $transfer->update();

        if ($transfer->confirmation_code != $confirmation_code) {
            if ($transfer->confirmation_attempts_count >= self::OPERATION_CONFIRMATION_CODE_ATTEMPTS_MAX) {
                $transfer->status = self::CONFIRMATION_CODE_ATTEMPTS_OVERFLOW;
                $transfer->update();

                return ['success' => false, 'error' => ['error' => 'CONFIRMATION_CODE_ATTEMPTS_OVERFLOW']];
            }

            $attempts_left = self::OPERATION_CONFIRMATION_CODE_ATTEMPTS_MAX - $transfer->confirmation_attempts_count;

            return ['success' => false, 'error' => ['error' => 'WRONG_CONFIRMATION_CODE', 'attempts_left' => $attempts_left]];
        }

        $transfer->status = self::CONFIRMED;
        $transfer->update();

        return ['success' => true, 'data' => ['code' => $transfer->code]];
    }

    /**
     * Подтверждение запроса получателем кодом протекции
     * @param $to
     * @param $operation_key
     * @param $code
     * @return array
     */
    static public function process($to, $operation_key, $code) {
        $transfer = P2PTransfer::where([
            'to' => $to,
            'operation_key' => $operation_key
        ])->first();

        if (is_null($transfer)) {
            return ['success' => false, 'error' => 'OPERATION_NOT_FOUND'];
        }

        if ($transfer->status != self::CONFIRMED) {
            $errors = [
                self::IN_PROGRESS => 'OPERATION_NOT_YET_CONFIRMED',
                self::SUCCESS => 'ALREADY_PERFORMED',
                self::CANCELLED => 'OPERATION_CANCELLED',
                self::NOT_ENOUGH_PROFIT => 'OPERATION_NOT_ENOUGH_PROFIT',
                self::EXPIRED => 'OPERATION_EXPIRED',
                self::CONFIRMATION_CODE_ATTEMPTS_OVERFLOW => 'OPERATION_CONFIRMATION_CODE_ATTEMPTS_OVERFLOW',
                self::UNKNOWN_ERROR => 'UNKNOWN_ERROR'
            ];
            return ['success' => false, 'error' =>$errors[$transfer->status]];
        }

        // FIXME: это костыль. если будут очереди, нужно устанавливать EXPIRED в задаче очереди
        if (strtotime($transfer->created_at) < time() - self::OPERATION_TIMEOUT_SEC) {
            $transfer->status = self::EXPIRED;
            $transfer->update();

            return ['success' => false, 'error' => 'OPERATION_EXPIRED'];
        }

        $transfer->attempts_count += 1;

        // wrong code
        if ($transfer->code != $code) {
            if ($transfer->attempts_count == self::OPERATION_PROTECTION_CODE_ATTEMPTS_MAX) {
                $transfer->status = self::PROTECTION_CODE_ATTEMPTS_OVERFLOW;

                $transfer->update();

                return ['success' => false, 'error' => 'OPERATION_ATTEMPTS_OVERFLOW'];
            }

            $transfer->update();

            return [
                'success' => false, 'error' => 'WRONG_CODE'
            ];
        }

        $res = PartnerApi::AddTransferEvent($transfer->from, $transfer->to, $transfer->amount, $transfer->operation_key);

        if (!empty($res['errors']) && $res['errors']['error'] == 'NOT_ENOUGH_PROFIT') {
            $transfer->status = self::NOT_ENOUGH_PROFIT;
            $transfer->update();

            return ['success' => false, 'error' => 'OPERATION_NOT_ENOUGH_PROFIT'];
        }

        if (!empty($res['errors']) && $res['errors']['error'] == 'REQUEST_ID_ALREADY_EXISTS') {
            $transfer->status = self::UNKNOWN_ERROR;
            $transfer->update();

            return ['success' => false, 'error' => 'OPERATION_DUPLICATE'];
        }

        // erh...
        if (isset($res['response']) && $res['response'] == 'ok') {
            $transfer->status = self::SUCCESS;
            $transfer->update();

            return ['success' => true];
        } else {
            $transfer->status = self::UNKNOWN_ERROR;
            $transfer->update();

            return ['success' => false, 'error' => 'UNKNOWN_ERROR'];
        }
    }

    /**
     * Отправитель отзывает запрос на перевод
     * @param $from
     * @param $operation_key
     * @return array
     */
    static public function cancel($from, $operation_key) {
        $transfer = P2PTransfer::where([
            'from' => $from,
            'operation_key' => $operation_key
        ])->first();

        if (is_null($transfer)) {
            return ['error' => 'OPERATION_NOT_FOUND'];
        }        

        if (!in_array($transfer->status, [self::CONFIRMED, self::IN_PROGRESS])) {
            return ['error' => 'OPERATION_ALREADY_PROCESSED'];
        }

        $transfer->status = self::CANCELLED;
        $transfer->update();

        return ['success' => true];
    }
}
