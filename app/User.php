<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
	protected $hidden = ['remember_token'];
	protected $guarded = ['id', 'remember_token'];

	public function phone() {
		return $this->HasOne('App\Models\phone', 'user_id', 'id');
	}
}
