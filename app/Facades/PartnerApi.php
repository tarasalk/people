<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class PartnerApi extends Facade{
    protected static function getFacadeAccessor() {
        return 'PartnerApi';
    }
}