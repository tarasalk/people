<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class CenomarketApi extends Facade {
    protected static function getFacadeAccessor() {
        return 'CenomarketApi';
    }
}