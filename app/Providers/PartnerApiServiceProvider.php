<?php

namespace App\Providers;

use App\Helpers\PartnerApiHelper;
use Illuminate\Support\ServiceProvider;

class PartnerApiServiceProvider extends ServiceProvider{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Register the application services.
     * @return void
     */
    public function register()
    {
        $this->app->singleton('PartnerApi', function ($app) {
            return new PartnerApiHelper($app->make('config')->get('app.partner_api.host'));
        });
    }

    public function provides() {
        return ['PartnerApi'];
    }
}