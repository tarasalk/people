<?php

namespace App\Providers;

use App\Helpers\CenomarketApiHelper;
use Illuminate\Support\ServiceProvider;

class CenomarketApiServiceProvider extends ServiceProvider {
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     * @return void
     */
    public function register()
    {
        $this->app->singleton('CenomarketApi', function ($app) {
            return new CenomarketApiHelper($app->make('config')->get('app.cenomarket_api.host'));
        });
    }

    public function provides() {
        return ['CenomarketApi'];
    }
}