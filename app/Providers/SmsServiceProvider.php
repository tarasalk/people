<?php

namespace App\Providers;

use App\Helpers\SmsHelper;
use Illuminate\Support\ServiceProvider;

class SmsServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     * @return void
     */
    public function register()
    {
        $this->app->singleton('Sms', function ($app) {
            return new SmsHelper($app->make('config')->get('app.sms_api.host'));
        });
    }

    public function provides() {
        return ['Sms'];
    }
}
