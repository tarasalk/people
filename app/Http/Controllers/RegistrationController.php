<?php

namespace App\Http\Controllers;

use App\Helpers\JsonWrapper;
use App\Models\Phone;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class RegistrationController extends Controller {
	protected function createUser(Request $request) {
		$country = $request->input('country');
		$phone = $request->input('phone');
		$first_name = $request->input('first_name', '');
		$last_name = $request->input('last_name', '');

		$validator = Validator::make($request->all(), [
			'country' => 'required',
			'phone' => 'required',
		]);
		if ($validator->fails())
			return JsonWrapper::error('wrong_params');

		$oPhone = Phone::whereRaw('country = ? && phone = ?', [$country, $phone])->first();
		if (isset($oPhone)) {
			if ($oPhone->activate_status == 1)
				$errorMessage = 'phone_activate';
			else
				$errorMessage = 'phone_exists';

			return JsonWrapper::error($errorMessage);
		}

		$oUser = new User([
			'first_name' => $first_name,
			'last_name' => $last_name,
			'password' => 1111,
			'vk_id' => $request->input('vk_id', '')
		]);
		$oUser->save();

		$oPhone = new Phone([
			'country' => $country,
			'phone' => $phone,
			'activate_code' => 1111
		]);
		$oUser->phone()->save($oPhone);

		// TODO смс

		return JsonWrapper::success(['user_id' => $oUser->id]);
	}

	protected function confirmPhone(Request $request) {
		$country = $request->input('country');
		$phone = $request->input('phone');
		$code = $request->input('code');

		$validator = Validator::make($request->all(), [
			'country' => 'required',
			'phone' => 'required',
			'code' => 'required'
		]);

		if ($validator->fails())
			return JsonWrapper::error('wrong_params');

		$oPhone = Phone::whereRaw('country = ? && phone = ?', [$country, $phone])->first();
		if (isset($oPhone)) {
			if ($oPhone->activate_status == 1)
				return JsonWrapper::error('phone_already_activate');

			if ($oPhone->activate_code == $code) {
				$oPhone->activate_status = 1;
				$oPhone->save();

				return JsonWrapper::success(['user_id' => $oPhone->user_id]);
			} else {
				$oPhone->activate_attempts++;
				return JsonWrapper::error('incorrect_code');
			}
		} else return JsonWrapper::error('phone_not_found');
	}

	protected function recoveryPassword(Request $request) {
		$country = $request->input('country');
		$phone = $request->input('phone');

		$validator = Validator::make($request->all(), [
			'country' => 'required',
			'phone' => 'required'
		]);

		if ($validator->fails())
			return JsonWrapper::error('wrong_params');

		$oPhone = Phone::whereRaw('country = ? && phone = ?', [$country, $phone])->first();
		if (isset($oPhone)) {
			$oUser = User::whereRaw('id = ?', [$oPhone->user_id])->first();
			if (isset($oUser)) {

				// TODO $oUser->password = rand(1000, 9999);
				$oUser->password = 2222;
				$oUser->save();

				return JsonWrapper::success(['user_id' => $oPhone->user_id]);
			}
		}

		return JsonWrapper::error('phone_not_found');
	}

	protected function sendCodeConfirmPhone(Request $request) {
		$country = $request->input('country');
		$phone = $request->input('phone');

		$validator = Validator::make($request->all(), [
			'country' => 'required',
			'phone' => 'required'
		]);

		if ($validator->fails())
			return JsonWrapper::error('wrong_params');

		// TODO проверка что телефон уже активирован

		$oPhone = Phone::whereRaw('country = ? && phone = ?', [$country, $phone])->first();
		if (isset($oPhone)) {
            // TODO генерация кода
			// TODO отправка смс

			return JsonWrapper::success();
		}

		return JsonWrapper::error('phone_not_found');
	}
}
