<?php

namespace App\Http\Controllers;

use App\Helpers\JsonWrapper;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Laravel\Socialite\Facades\Socialite;

class AuthController extends Controller
{
	public function auth(Request $request) {
		$redirect = $request->input('redirect');
		$user_id = $request->input('user_id');

		$oUser = Auth::loginUsingId($user_id);

		if (isset($oUser)) {
			$result = true;
			setcookie('user_id', $oUser->id, time() + 60*60);
		}
		else $result = false;
		return redirect($redirect.'?auth='.$result);
	}

    protected function authVk(Request $request) {
        $redirect = $request->input('redirect', route('main'));
        session(['authVkRedirect' => $redirect]);

        return Socialite::with('vkontakte')->stateless()->redirect();
    }

    protected function authVkCallback() {
        $user = Socialite::with('vkontakte')->stateless()->user();
        $oUser = User::where('vk_id', '=', $user['uid'])->first();

        if (!isset($oUser)) {
            $redirectParams = http_build_query([
                'error' => 'user_not_found',
                'vk_id' => $user['uid'],
                'first_name' => $user['first_name'],
                'last_name' => $user['last_name']
            ]);

        }
        else {
			Auth::loginUsingId($oUser->id);
            $redirectParams = http_build_query([
                'auth' => true,
                'user_id' => $oUser->id
            ]);
        }

        return redirect(session('authVkRedirect').'?'.$redirectParams);
    }

    public function check(Request $requests) {
		$redirect = $requests->input('redirect');
        if (empty($redirect))
            return JsonWrapper::error(['wrong_redirect']);

		if (Auth::check()) {
			$user_id = Auth::user()->id;
            return redirect($redirect.'/?auth_user_id='.$user_id);
		}

		return redirect($redirect.'?error=user_not_found');
	}

	public function logout(Request $request) {
		$redirect = $request->input('redirect');

		Auth::logout();

		return redirect($redirect);
	}
}