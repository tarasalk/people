<?php

namespace App\Http\Controllers;

use App\Helpers\JsonWrapper;
use App\Models\UserPassport;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class PassportController extends Controller
{
    protected function getData(Request $request) {
		if (!$request->has('user_id'))
			return JsonWrapper::error('wrong_user_id');

		$user_id = $request->input('user_id');
		$oUserPassport = UserPassport::where('user_id', '=', $user_id)->first();

		if (isset($oUserPassport))
			return JsonWrapper::success(['data' => $oUserPassport]);
		else
			return JsonWrapper::error(['user_passport_not_found']);
	}

	protected function createOrEditData(Request $request) {
		$validator = Validator::make($request->all(), [
			'user_id' => 'required',
			'data' => 'required'
		]);

		if ($validator->fails())
			return JsonWrapper::validation($validator->messages());

		$user_id = $request->input('user_id');
		$data = $request->input('data'); // TODO добавить валидацию?

		$oUserPassport = UserPassport::firstOrCreate(['user_id' => $user_id]);
		$oUserPassport->update($data);

        if ($oUserPassport->save())
		    return JsonWrapper::success(['user' => $oUserPassport]);

        return JsonWrapper::error('undefined_error');
	}
}
