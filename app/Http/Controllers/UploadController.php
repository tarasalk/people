<?php

namespace App\Http\Controllers;

use App\Helpers\JsonWrapper;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

class UploadController extends Controller
{
	const PATH_PASSPORT_SCANS = 'upload/passportScan/';

	protected function test(Request $request) {
		$file = $request->file('file');

		$normalMimeType = [
			'jpg' => 'image/jpeg',
			'png' => 'image/png',
			'gif' => 'image/gif'
		];

		if (!in_array($file->getMimeType(), $normalMimeType))
			return JsonWrapper::error('undefined_format');

		$extension = $file->getClientOriginalExtension();
		$fileName = rand(1000000, 99999999).'.'.$extension;
		$file->move(self::UPLOAD_PATH, $fileName);

		return JsonWrapper::success(['title' => $fileName, 'path' => self::UPLOAD_PATH.$fileName]);
	}

	protected function passportScans(Request $request) {
		if ($request->hasFile('pass_scan1'))
			$requestFileName = 'pass_scan1';
		elseif ($request->hasFile('pass_scan2'))
			$requestFileName = 'pass_scan2';
		elseif ($request->hasFile('proxy_scan1'))
			$requestFileName = 'proxy_scan1';
		elseif ($request->hasFile('proxy_scan2'))
			$requestFileName = 'proxy_scan2';
		else
			return JsonWrapper::error('undefined_scan');

		$file = $request->file($requestFileName);

		$normalMimeType = [
			'jpg' => 'image/jpeg',
			'png' => 'image/png',
			'gif' => 'image/gif'
		];

		if (!in_array($file->getMimeType(), $normalMimeType))
			return JsonWrapper::error('undefined_mime');

		$extension = $request->input('extension');
		$fileName = sha1($file->getFilename().rand(1000,9999)).'.'.$extension;
		$file->move(self::PATH_PASSPORT_SCANS, $fileName);

		return JsonWrapper::success(['title' => $fileName, 'path' => self::PATH_PASSPORT_SCANS.$fileName]);
	}
}
