<?php

namespace App\Http\Controllers;

use App\Helpers\JsonWrapper;
use App\Models\P2PTransfer;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class P2PTransferController extends Controller
{
    const INSUFFICIENT_PARAMS = 'INSUFFICIENT_PARAMS';

    /**
     * Запрос на p2p-перевод
     */
    protected function request(Request $request) {
        $validation = Validator::make($request->all(), [
            'from' => 'required|integer',
            'to' => 'required|integer',
            'amount' => 'required|integer|min:1'
        ]);

        if ($validation->fails()) {
            return JsonWrapper::error(self::INSUFFICIENT_PARAMS);
        }

        $response = P2PTransfer::request($request->get('from'), $request->get('to'), $request->get('amount'));

        if (!$response['success']) {
            return JsonWrapper::error($response['error']);
        }

        return JsonWrapper::success([
            'operation_key' => $response['data']['operation_key']
        ]);
    }

    protected function confirmRequest(Request $request) {
        $validation = Validator::make($request->all(), [
            'from' => 'required|integer',
            'operation_key' => 'required|alpha_num',
            'confirmation_code' => 'required|integer'
        ]);

        if ($validation->fails()) {
            return JsonWrapper::error(self::INSUFFICIENT_PARAMS);
        }

        $response = P2PTransfer::confirmRequest($request->get('from'), $request->get('operation_key'), $request->get('confirmation_code'));

        if (!$response['success']) {
            return JsonWrapper::error($response['error']);
        }

        return JsonWrapper::success(['code' => $response['data']['code']]);
    }

    /**
     * Обработка кода протекции
     */
    protected function process(Request $request) {
        $validation = Validator::make($request->all(), [
            'user_id' => 'required|integer',
            'operation_key' => 'required|alpha_num',
            'code' => 'required|digits:4'
        ]);

        if ($validation->fails()) {
            return JsonWrapper::error(self::INSUFFICIENT_PARAMS);
        }

        $status = P2PTransfer::process($request->get('user_id'), $request->get('operation_key'), $request->get('code'));

        if (isset($status['error'])) {
            return JsonWrapper::error($status['error']);
        }

        return JsonWrapper::success($status['success']);
    }

    /**
     * Отменить перевод
     */
    protected function cancel(Request $request) {
        $validation = Validator::make($request->all(), [
            'from' => 'required|integer',
            'operation_key' => 'required|alpha_num'
        ]);

        if ($validation->fails()) {
            return JsonWrapper::error(self::INSUFFICIENT_PARAMS);
        }

        $from = $request->get('from');
        $operation_key = $request->get('operation_key');

        $status = P2PTransfer::cancel($from, $operation_key);

        if (isset($status['error'])) {
            return JsonWrapper::error($status['error']);
        }

        return JsonWrapper::success($status['success']);
    }



    /**
     * Получить входящие, ожидающие обработки, переводы
     */
    protected function getIncomingPendingTransfers(Request $request) {
        $validation = Validator::make($request->all(), [
            'user_id' => 'required|integer',
        ]);

        if ($validation->fails()) {
            return JsonWrapper::error(self::INSUFFICIENT_PARAMS);
        }

        $transfers = P2PTransfer::where('to', '=', $request->get('user_id'))
                                ->where('status', '=', P2PTransfer::CONFIRMED)
                                ->orderBy('created_at', 'desc');

        return JsonWrapper::success([
            'transfers' => $transfers->get(), 
            'sum' => $transfers->sum('amount')
        ]);
    }

    /**
     * Получить исходящие, ожидающие обработки, переводы
     */
    protected function getOutgoingPendingTransfers(Request $request) {
        $validation = Validator::make($request->all(), [
            'user_id' => 'required|integer',
        ]);

        if ($validation->fails()) {
            return JsonWrapper::error(self::INSUFFICIENT_PARAMS);
        }

        $transfers = P2PTransfer::where('from', '=', $request->get('user_id'))
                                ->where('status', '=', P2PTransfer::CONFIRMED)
                                ->orderBy('created_at', 'desc');

        return JsonWrapper::success([
            'transfers' => $transfers->get(), 
            'sum' => $transfers->sum('amount')
        ]);
    }



    /**
     * Получить историю переводов по пользователю
     */
    protected function getHistory(Request $request) {
        $user_id = $request->get('user_id');

        $history = P2PTransfer::where('to', '=', $user_id)
                            ->orWhere('from', '=', $user_id)
                            ->where('status', '=', P2PTransfer::SUCCESS)
                            ->get();

        return JsonWrapper::success(['history' => $history]);
    }
}
