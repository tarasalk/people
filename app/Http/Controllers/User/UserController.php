<?php

namespace App\Http\Controllers\User;

use App\Helpers\JsonWrapper;
use App\Models\Phone;
use App\Models\UserPassport;
use App\Models\UserRoles;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UserController extends Controller
{
	/**
	 * возвращает расширенную информацию о пользователях
	 */
	public function getUsers(Request $request) {
		$user_ids = $request->input('user_ids');
		$aFields = $request->input('fields', []);

		if (empty($user_ids) || !is_array($user_ids))
			return JsonWrapper::error('wrong_user_ids');

		$result = [];

		$aUsers = User::whereIn('id', $user_ids)->get();
		foreach ($aUsers as $oUser) {
			$result[$oUser->id]['user_id'] = $oUser->id;
			$result[$oUser->id]['base']['vk_id'] = $oUser->vk_id;
			$result[$oUser->id]['base']['first_name'] = $oUser->first_name;
			$result[$oUser->id]['base']['last_name'] = $oUser->last_name;

            if (in_array('avatar', $aFields))
                $result[$oUser->id]['base']['avatar'] = $oUser->avatar;
		}

		$aUsersPassport = UserPassport::whereIn('user_id', $user_ids)->get();
		foreach ($aUsersPassport as $oUserPassport) {
			$result[$oUserPassport->user_id]['passport']['first_name'] = $oUserPassport->first_name;
			$result[$oUserPassport->user_id]['passport']['last_name'] = $oUserPassport->last_name;
			$result[$oUserPassport->user_id]['passport']['middle_name'] = $oUserPassport->middle_name;
		}

		$aUsersPhone = Phone::whereIn('user_id', $user_ids)->get();
		foreach ($aUsersPhone as $oUserPhone) {
			$result[$oUserPhone->user_id]['phone']['country'] = $oUserPhone->country;
			$result[$oUserPhone->user_id]['phone']['phone'] = $oUserPhone->phone;
			$result[$oUserPhone->user_id]['phone']['activate_status'] = $oUserPhone->activate_status;
		}

		$aUsersRole = UserRoles::whereIn('user_id', $user_ids)->get();
		foreach ($aUsersRole as $oUsersRole) {
			$result[$oUsersRole->user_id]['roles'][$oUsersRole->role_id]['id'] = $oUsersRole->role_id;
			$result[$oUsersRole->user_id]['roles'][$oUsersRole->role_id]['name'] = $oUsersRole->role->name;
		}

		return JsonWrapper::success(['users' => $result]);
	}

	/**
	 * зачатки метода поиска людей по параметрам
	 * search_params: first_name, last_name, country, phone
	 */
	public function search(Request $request) {
		$country = $request->input('country');
		$phone = $request->input('phone');

		if (empty($country) || empty($phone))
			return JsonWrapper::error('wrong_params');

		$aPhone = Phone::select('user_id')->whereRaw('country = ? && phone = ?', [$country, $phone])->get();
		foreach($aPhone as $key => &$oPhone) {
			if ($request->has('first_name') && $oPhone->user->first_name != $request->input('first_name'))
				unset($aPhone[$key]);

			if ($request->has('last_name') && $oPhone->user->last_name != $request->input('last_name'))
				unset($aPhone[$key]);

			if ($request->has('user_id') && $oPhone->user->id != $request->input('user_id'))
				unset($aPhone[$key]);

			if ($request->has('password') && $oPhone->user->password != $request->input('password'))
				unset($aPhone[$key]);
		}

		return JsonWrapper::success(['users' => $aPhone->pluck('user_id')->toArray()]);
	}

	public function getEntity(Request $request) {
		$user_id = $request->input('user_id');

		if (!is_numeric($user_id))
			return JsonWrapper::error('wrong_user_id');

		return JsonWrapper::success(['user' => User::find($user_id)]);
	}

	public function getRoles(Request $request) {
		$user_id = $request->input('user_id');

		if (!is_numeric($user_id))
			return JsonWrapper::error('wrong_user_id');

		$aUserRoles = DB::table('user_roles')
			->leftJoin('roles','user_roles.role_id','=','roles.id')
			->select('roles.*')
			->where('user_roles.user_id', $user_id)
			->orderBy('roles.id')
			->get();

		return JsonWrapper::success(['roles' => $aUserRoles]);
	}

	public function setRole(Request $request) {
		$role_id = $request->input('role_id');
		$user_id = $request->input('user_id');

		if (empty($role_id) || empty($user_id))
			return JsonWrapper::error('wrong_params');

		$oUserRole = UserRoles::whereRaw('user_id = ? && role_id = ?', [$user_id, $role_id])->first();
		if (isset($oUserRole))
			return JsonWrapper::error('role_already_set');
		else {
			UserRoles::create(['user_id' => $user_id, 'role_id' => $role_id]);

			return JsonWrapper::success(['user_id' => $user_id]);
		}
	}

    /**
     * TODO Cache?
     * возвращает количество всех зарегистрированных пользователей
     */
    public function getCount() {
        return JsonWrapper::success(User::all()->count());
    }
}