<?php
use App\Facades\CenomarketApi;

Route::get('/', ['as' => 'main', function () {
    return view('welcome');
}]);

Route::group(['prefix' => 'auth', 'middleware' => ['web']], function () {
	// авторизация по user_id
	Route::any('/', ['uses' => 'AuthController@auth']);

	// существует ли пользователь
	Route::any('/check', ['uses' => 'AuthController@check']);

	// авторизация через вк
	Route::get('/vk', ['as' => 'authVk', 'uses' => 'AuthController@authVk']);
	Route::get('/vk/callback', 'AuthController@authVkCallback');

	Route::any('/logout', ['uses' => 'AuthController@logout']);
});

// регистрация
Route::group(['prefix' => 'registration'], function () {
	Route::post('/create', ['uses' => 'RegistrationController@createUser']);
	Route::post('/confirmPhone', ['uses' => 'RegistrationController@confirmPhone']);
	Route::post('/recoveryPassword', ['uses' => 'RegistrationController@recoveryPassword']);
	Route::post('/sendCodeConfirmPhone', ['uses' => 'RegistrationController@sendCodeConfirmPhone']);
});

// паспортные данные
Route::group(['prefix' => 'passport'], function() {
	Route::any('/data/get', ['uses' => 'PassportController@getData']);
	Route::any('/data/createOrEdit', ['uses' => 'PassportController@createOrEditData']);
});

Route::group(['prefix' => 'upload'], function() {
	Route::post('/passportScans', ['uses' => 'UploadController@passportScans']);
});

Route::group(['namespace' => 'User', 'prefix' => 'user'], function() {
	Route::post('/get', ['uses' => 'UserController@getUsers']);
	Route::post('/getEntity', ['uses' => 'UserController@getEntity']);
	Route::post('/search', ['uses' => 'UserController@search']);

	Route::post('/getRoles', ['uses' => 'UserController@getRoles']);
	Route::post('/setRole', ['uses' => 'UserController@setRole']);

	Route::post('/count', ['uses' => 'UserController@getCount']);
});



/**
 * Внутренние переводы (p2p - person to person)
 */
Route::group(['prefix' => 'p2p'], function () {
	// запросить перевод
	Route::post('/request', ['uses' => 'P2PTransferController@request']);
	// ввод смс-кода подтверждения перевода
	Route::post('/confirmRequest', ['uses' => 'P2PTransferController@confirmRequest']);
	// обработать перевод
	Route::post('/process', ['uses' => 'P2PTransferController@process']);
	// отменить перевод
	Route::post('/cancel', ['uses' => 'P2PTransferController@cancel']);


	// получить входящие ожидающие обработки переводы
	Route::get('/getIncomingPendingTransfers', ['uses' => 'P2PTransferController@getIncomingPendingTransfers']);
	// получить исходящие ожидающие обработки переводы
	Route::get('/getOutgoingPendingTransfers', ['uses' => 'P2PTransferController@getOutgoingPendingTransfers']);

	// история
	Route::get('/getHistory', ['uses' => 'P2PTransferController@getHistory']);
});
