<?php

namespace App\Helpers;

use App\Facades\CenomarketApi;
use Illuminate\Support\Facades\Config;
use GuzzleHttp\Client;

class SmsHelper {
    protected $host;

    const SENDER_CENOBOY = "Cenoboy";

    const COUNTRY_RU = "RU";

    public function __construct($host) {
        $this->host = $host;
    }

    /**
     * @param $phone
     * @param $message
     * @param string $sender
     * @param string $country_code
     * @return array
     */
    public function SendSms($phone, $message, $sender = self::SENDER_CENOBOY, $country_code = self::COUNTRY_RU) {
        if (!Config::get('app.send_sms')) {
            return false;
        }

        try {
            $url = $this->host . '/index.php/send_sms';

            $post_params = array(
                'phone' => $phone,
                'message' => $message,
                'sender' => $sender,
                'country' => $country_code
            );

	    $response = json_decode((new Client())->post($url . '?' . http_build_query($post_params))->getBody()->getContents(), true);

            if ($response['status'] !== 'ok')
                throw new \Exception(implode(' ', $response['errors']));

            $output = array('status' => 'ok', 'result' => $response['response']);
        } catch (\Exception $e) {
            $output = array('status' => 'error', 'phone' => $phone, 'error' => $e->getMessage());
        }

        return $output;
    }

    public function SendSmsByUserId($user_id, $message) {
        // TODO: insert CenomarketApi through DI?
        $phone = CenomarketApi::getUserPhone($user_id);

        if (!strlen($phone)) {
            return false;
        }

        try {
            $sending_result = $this->SendSms($phone, $message);    
        } catch(\Exception $e) {
            return false;
        }

        if ($sending_result['status'] != 'ok') {
            return false;
        }

        return true;
    }
}
