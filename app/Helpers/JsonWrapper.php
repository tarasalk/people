<?php

namespace App\Helpers;


class JsonWrapper {
	const SUCCESS = 'ok';
	const ERROR = 'error';

	private static function send($status = self::ERROR, $response = []) {
		return ['status' => $status, 'response' => $response];
	}

	public static function success($content = []) {
		return self::send(self::SUCCESS, $content);
	}

	public static function error($content = []) {
		return self::send(self::ERROR, $content);
	}

	/**
	 * ошибки валидации
	 * @param $content
	 * @return array
	 */
	/*public static function validation($content) {
		$response = [
			'status' => JsonWrapper::ERROR,
			'validation' => $content
		];

		return $response;
	}*/
}