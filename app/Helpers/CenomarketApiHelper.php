<?php

namespace App\Helpers;

use GuzzleHttp\Client;

class CenomarketApiHelper {
    protected $host;

    public function __construct($host) {
        $this->host = $host;
    }

    public function getUserInfo($user_id) {
        $url = $this->host . '/api/getUserInfo?ids=' . $user_id;

        return json_decode((new Client())->get($url)->getBody()->getContents(), true);
    }

    // TODO: переместить в getUserInfo?
    public function getUserProfit($user_id) {
        $nonce = time();
        $salt = '#O!F_WG|KT4';

        // FIXME: inject Config dependency in __construct, maybe?
        $res = json_decode((new Client())->get($this->host . '/api/getUserProfit?' . http_build_query([
            'user_id' => $user_id,
            'nonce' => $nonce,
            'key' => md5(md5($user_id . $nonce) . $salt)
        ]))->getBody()->getContents(), true);

        return isset($res['response']['profit']) ? $res['response']['profit']: 0;
    }

    public function getUserPhone($user_id) {
        $users = $this->getUserInfo($user_id);

        return $users['response'][$user_id]['phone'];
    }
}