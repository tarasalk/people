<?php

namespace App\Helpers;

use GuzzleHttp\Client;

// TODO: скопипастить сюда все методы с лайвстритовского classes/modules/partnerapi/PartnerApi.class.php
class PartnerApiHelper {
    private $host;

    public function __construct($host) {
        $this->host = $host;
    }

    public function AddTransferEvent($from, $to, $amount, $operation_key) {
        $url = $this->host . '/events/add/transfer';

        return $res = json_decode((new Client())->post($url . '?' . http_build_query([
            'from' => $from,
            'to' => $to,
            'amount' => $amount,
            'request_id' => md5($operation_key)
        ]))->getBody()->getContents(), true);
    }
}