<?php

use App\Facades\CenomarketApi;
use App\Facades\PartnerApi;
use App\Models\P2PTransfer;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class P2PTest extends TestCase
{
    use DatabaseMigrations;

    const BASE_ROUTE = '/p2p';

    public function testRequest_Successful() {
        $amount = 10000;
        $from = 1;
        $to = 2;

        CenomarketApi::shouldReceive('getUserProfit')->with($from)->andReturn($amount);
        CenomarketApi::shouldReceive('getUserPhone')->with($from)->andReturn('79876543210');

        $request = $this->post(self::BASE_ROUTE . '/request', [
            'from' => $from, 'to' => $to, 'amount' => $amount
        ])->seeJsonStructure([
            'status', 'response' => ['operation_key']
        ]);

        return json_decode($request->response->content(), true);
    }

    public function testRequest_NotEnoughProfit() {
        $amount = 10000;
        $from = 1;
        $to = 2;

        CenomarketApi::shouldReceive('getUserProfit')->with($from)->andReturn(0);
        CenomarketApi::shouldReceive('getUserPhone')->with($from)->andReturn('79876543210');

        $this->post(self::BASE_ROUTE . '/request', [
            'from' => $from, 'to' => $to, 'amount' => $amount
        ])->seeJson([
            "status" => "error",
            "response" => "NOT_ENOUGH_PROFIT",
        ]);
    }

    public function testConfirmRequest_Successful() {
        $request = $this->testRequest_Successful();

        $from = 1;
        $operation_key = $request['response']['operation_key'];

        $transfer = P2PTransfer::where('operation_key', '=', $operation_key)->first();
        $request = $this->post(self::BASE_ROUTE . '/confirmRequest', [
            'from' => $from,
            'operation_key' => $operation_key,
            'confirmation_code' => $transfer->confirmation_code
        ])->seeJsonEquals([
            "response" => [
                "code" => $transfer->code,
            ],
            "status" => "ok",
        ]);

        $transfer = $transfer->fresh();
        $this->assertEquals(P2PTransfer::CONFIRMED, $transfer->status);

        return json_decode($request->response->content(), true);
    }

    public function testConfirmRequest_ConfirmationCodeAttemptsOverflow() {
        $request = $this->testRequest_Successful();

        $from = 1;
        $operation_key = $request['response']['operation_key'];

        // P2PTransfer::OPERATION_CONFIRMATION_CODE_ATTEMPTS_MAX - 1 раз отправляем неверный код
        for ($i = 0; $i < P2PTransfer::OPERATION_CONFIRMATION_CODE_ATTEMPTS_MAX - 1; $i++) {
            $this->post(self::BASE_ROUTE . '/confirmRequest', [
                'from' => $from,
                'operation_key' => $operation_key,
                'confirmation_code' => $i
            ])->seeJsonEquals([
                "status" => "error",
                "response" => [
                    "error" => "WRONG_CONFIRMATION_CODE",
                    "attempts_left" => P2PTransfer::OPERATION_CONFIRMATION_CODE_ATTEMPTS_MAX - ($i + 1)
                ]
            ]);
        }

        // P2PTransfer::OPERATION_CONFIRMATION_CODE_ATTEMPTS_MAX-ый раз отправляем код, получаем ошибку
        $this->post(self::BASE_ROUTE . '/confirmRequest', [
            'from' => $from,
            'operation_key' => $operation_key,
            'confirmation_code' => $i
        ])->seeJsonEquals([
            "status" => "error",
            "response" => [
                "error" => P2PTransfer::CONFIRMATION_CODE_ATTEMPTS_OVERFLOW
            ]
        ]);

        $transfer = P2PTransfer::where(['operation_key' => $operation_key])->first();
        $this->assertEquals(P2PTransfer::CONFIRMATION_CODE_ATTEMPTS_OVERFLOW, $transfer->status);
    }

    public function testProcess_Successful() {
        $confirmation_status = $this->testConfirmRequest_Successful();

        $to = 2;
        $protection_code = $confirmation_status['response']['code'];

        $transfer = P2PTransfer::where(['code' => $protection_code, 'to' => $to])->first();

        PartnerApi::shouldReceive('AddTransferEvent')
                    ->with($transfer->from, $to, $transfer->amount, $transfer->operation_key)
                    ->andReturn(['response' => 'ok', 'errors' => []]);

        $request = $this->post(self::BASE_ROUTE . '/process', [
            'user_id' => $to,
            'operation_key' => $transfer->operation_key,
            'code' => $protection_code
        ])->seeJsonEquals([
            'status' => 'ok',
            'response' => true
        ]);
    }

    public function testProcess_NotEnoughProfit() {
        $confirmation_status = $this->testConfirmRequest_Successful();

        $to = 2;
        $protection_code = $confirmation_status['response']['code'];

        $transfer = P2PTransfer::where(['code' => $protection_code, 'to' => $to])->first();

        PartnerApi::shouldReceive('AddTransferEvent')
                    ->with($transfer->from, $to, $transfer->amount, $transfer->operation_key)
                    ->andReturn(['response' => '', 'errors' => ['error' => 'NOT_ENOUGH_PROFIT']]);

        $request = $this->post(self::BASE_ROUTE . '/process', [
            'user_id' => $to,
            'operation_key' => $transfer->operation_key,
            'code' => $protection_code
        ])->seeJsonEquals([
            "status" => "error",
            "response" => "OPERATION_NOT_ENOUGH_PROFIT",
        ]);
    }

    public function testProcess_OperationAttemptsOverflow() {
        $confirmation_status = $this->testConfirmRequest_Successful();

        $to = 2;
        $user_input_protection_code = '0000';

        $protection_code = $confirmation_status['response']['code'];
        $transfer = P2PTransfer::where(['code' => $protection_code, 'to' => $to])->first();

        PartnerApi::shouldReceive('AddTransferEvent')
                    ->with($transfer->from, $to, $transfer->amount, $transfer->operation_key)
                    ->andReturn(['response' => '', 'errors' => ['error' => 'NOT_ENOUGH_PROFIT']]);

        for ($i = 0; $i < P2PTransfer::OPERATION_PROTECTION_CODE_ATTEMPTS_MAX - 1; $i++) {
            $this->post(self::BASE_ROUTE . '/process', [
                'user_id' => $to,
                'operation_key' => $transfer->operation_key,
                'code' => $user_input_protection_code
            ])->seeJsonEquals([
                "status" => "error",
                "response" => "WRONG_CODE",
            ]);
        }

        $this->post(self::BASE_ROUTE . '/process', [
            'user_id' => $to,
            'operation_key' => $transfer->operation_key,
            'code' => $user_input_protection_code
        ])->seeJsonEquals([
            "status" => "error",
            "response" => "OPERATION_ATTEMPTS_OVERFLOW",
        ]);

        $transfer = $transfer->fresh();
        $this->assertEquals(P2PTransfer::PROTECTION_CODE_ATTEMPTS_OVERFLOW, $transfer->status);
    }

    public function testProcess_UnknownError() {
        $confirmation_status = $this->testConfirmRequest_Successful();

        $to = 2;
        $protection_code = $confirmation_status['response']['code'];

        $transfer = P2PTransfer::where(['code' => $protection_code, 'to' => $to])->first();

        PartnerApi::shouldReceive('AddTransferEvent')
                    ->with($transfer->from, $to, $transfer->amount, $transfer->operation_key)
                    ->andReturn(['response' => '', 'errors' => ['error' => 'UNKNOWN_ERROR']]);

        $this->post(self::BASE_ROUTE . '/process', [
            'user_id' => $to,
            'operation_key' => $transfer->operation_key,
            'code' => $protection_code
        ])->seeJsonEquals([
            "status" => "error",
            "response" => "UNKNOWN_ERROR",
        ]);

        $transfer = $transfer->fresh();
        $this->assertEquals(P2PTransfer::UNKNOWN_ERROR, $transfer->status);
    }
}
