<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RegistrationTest extends TestCase
{
	use DatabaseMigrations;

	public function testCreateUserEmptyParams()
    {
		$this->post('/registration/create', [])
			->seeJson([
				'status' => 'error',
				'response' => 'wrong_params'
			]);
    }

	public function testCreateUser_Valid() {
		$this->post('/registration/create', [
			'country' => 7,
			'phone' => '9625702875',
			'first_name' => 'Руслан',
			'last_name' => 'Самолетов'
		])
			->seeJson([
				'status' => 'ok',
				'response' => ['user_id' => 1]
			]);
	}

	public function testCreateUser_PhoneExists() {
		$this->post('/registration/create', [
			'country' => 7,
			'phone' => '9600646094',
			'first_name' => 'Тарас',
			'last_name' => 'Самоткин'
		])
			->seeJson([
				'status' => 'ok',
				'response' => ['user_id' => 1]
			]);

		$this->post('/registration/create', [
			'country' => 7,
			'phone' => '9600646094',
			'first_name' => 'Бульдог',
			'last_name' => 'Хаматкин'
		])
			->seeJson([
				'status' => 'error',
				'response' => 'phone_exists'
			]);
	}

	public function testConfirmPhone_EmptyParams() {
		$this->post('/registration/confirmPhone', [])
			->seeJson([
				'status' => 'error',
				'response' => 'wrong_params'
			]);
	}

	public function testConfirmPhone_Valid() {
		$this->testCreateUser_Valid();

		$this->post('/registration/confirmPhone', [
			'country' => 7,
			'phone' => '9625702875',
			'code' => '1111'
		])
			->seeJson([
				'status' => 'ok',
				'response' => ['user_id' => 1]
			]);
	}

	public function testConfirmPhone_PhoneNotFound() {
		$this->testCreateUser_Valid();

		$this->post('/registration/confirmPhone', [
			'country' => 7,
			'phone' => '9876543210',
			'code' => '12345'
		])
			->seeJson([
				'status' => 'error',
				'response' => 'phone_not_found'
			]);
	}

	public function testConfirmPhone_PhoneAlreadyActivate() {
		$this->testConfirmPhone_Valid();

		$this->post('/registration/confirmPhone', [
			'country' => 7,
			'phone' => '9625702875',
			'code' => '1111'
		])
			->seeJson([
				'status' => 'error',
				'response' => 'phone_already_activate'
			]);
	}

	public function testConfirmPhone_IncorrectCode() {
		$this->testCreateUser_Valid();

		$this->post('/registration/confirmPhone', [
			'country' => 7,
			'phone' => '9625702875',
			'code' => '1234'
		])
			->seeJson([
				'status' => 'error',
				'response' => 'incorrect_code'
			]);
	}

	public function testRecoveryPassword_EmptyParams() {
		$this->testConfirmPhone_Valid();

		$this->post('/registration/recoveryPassword', [])
			->seeJson([
				'status' => 'error',
				'response' => 'wrong_params'
			]);
	}

	public function testRecoveryPassword_PhoneNotFound() {
		$this->testConfirmPhone_Valid();

		$this->post('/registration/recoveryPassword', [
			'country' => 7,
			'phone' => '9876543210'
		])
			->seeJson([
				'status' => 'error',
				'response' => 'phone_not_found'
			]);
	}

	public function testRecoveryPassword_Valid() {
		$this->testConfirmPhone_Valid();

		$this->post('/registration/recoveryPassword', [
			'country' => 7,
			'phone' => '9625702875'
		])
			->seeJson([
				'status' => 'ok',
				'response' => ['user_id' => 1]
			]);
	}
}
