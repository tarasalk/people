<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserTest extends TestCase
{
	use DatabaseMigrations;

	public function testUserGet_EmptyParams()
	{
		$this->post('/user/get', [])
			->seeJson([
				'status' => 'error',
				'response' => 'wrong_user_ids'
			]);
	}

    public function testUserGet_ValidOneUser()
    {
		$auth = new RegistrationTest();
		$auth->setUp();
		$auth->testCreateUser_Valid();

		$this->post('/user/get', [
			'user_ids' => [1]
		])
			->seeJson([
				'status' => 'ok',
				'response' => [
					'users' => [
						'1' => [
							'base' => [
								'id' => 1,
								'vk_id' => '',
								'first_name' => 'Руслан',
								'last_name' => 'Самолетов'
							],
							'phone' => [
								'country' => 7,
								'phone' => '9625702875'
							]
						]
					]
				]
			]);
    }
}
