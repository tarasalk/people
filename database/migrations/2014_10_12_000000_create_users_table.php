<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('users', function (Blueprint $table) {
			$table->increments('id')->unique();
			$table->string('vk_id');
			$table->string('password');
			$table->string('first_name');
			$table->string('last_name');
			$table->dateTime('birthday');
			$table->enum('gender', ['male', 'female', 'undefined']);
			$table->string('city_id');
			$table->string('avatar');
			$table->bigInteger('balance');
			$table->bigInteger('frozen_balance');
			$table->bigInteger('click_balance');
			$table->rememberToken();
			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
