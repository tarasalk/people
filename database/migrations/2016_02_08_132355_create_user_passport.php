<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPassport extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_passport', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id');
			$table->string('first_name');
			$table->string('last_name');
			$table->string('middle_name');
			$table->date('birthday');

			$table->string('pass_series'); // серия паспорта
			$table->string('pass_number'); // номер паспорта
			$table->string('pass_issued'); // где и кем выдан
			$table->date('pass_issued_date'); // когда выдан
			$table->string('unit_code'); // код подразделения
			$table->string('gender'); // пол

			// сканы паспорта и доверенности
			$table->string('pass_scan1');
			$table->string('pass_scan2');
			$table->string('proxy_scan1');
			$table->string('proxy_scan2');

			// место рождения
			$table->string('birth_republic');
			$table->string('birth_city');

			// адрес регистрации
			$table->string('registration_republic');
			$table->string('registration_city');
			$table->string('registration_street');
			$table->string('registration_house');
			$table->string('registration_corps');
			$table->string('registration_room');
			$table->string('registration_date');

			// адрес для писем и документов
			$table->string('mail_republic');
			$table->string('mail_city');
			$table->string('mail_street');
			$table->string('mail_house');
			$table->string('mail_corps');
			$table->string('mail_room');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_passport');
	}
}
