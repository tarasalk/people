<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhone extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('phones', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id');
			$table->integer('country'); // что подставлять перед номером телефона
			$table->integer('country_id');
			$table->string('phone');
			$table->integer('activate_code');
			$table->tinyInteger('activate_status');
			$table->tinyInteger('activate_attempts');
			$table->dateTime('date_activate');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('phones');
	}
}
