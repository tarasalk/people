<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateP2pTransfers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('p2p_transfers', function (Blueprint $table) {
            $table->increments('id');

            $table->string('operation_key', 40); // sha1 length

            $table->integer('from');
            $table->integer('to');
            $table->unsignedInteger('amount');

            // код подтверждения операции (подтверждает отправитель)
            $table->unsignedInteger('confirmation_code');
            $table->unsignedInteger('confirmation_attempts_count');

            // код протекции (подтверждает получатель)
            $table->unsignedInteger('code'); // код протекции
            $table->unsignedSmallInteger('attempts_count')->default(0); // кол-во попыток

            $table->enum('status', [
                'IN_PROGRESS',
                'CONFIRMED',
                'CONFIRMATION_CODE_ATTEMPTS_OVERFLOW',
                'SUCCESS',
                'CANCELLED',
                'REJECTED',
                'PROTECTION_CODE_ATTEMPTS_OVERFLOW',
                'EXPIRED',
                'UNKNOWN_ERROR'
            ])->default('IN_PROGRESS');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('p2p_transfers');
    }
}
