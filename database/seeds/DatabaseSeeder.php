<?php

use App\Models\Roles;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesSeeder::class);
    }
}

class RolesSeeder extends Seeder {
	public function run() {
		DB::table('roles')->truncate();

		Roles::create(['name' => 'admin', 'rus_name' => 'Админ']);
		Roles::create(['name' => 'client', 'rus_name' => 'Клиент']);
		Roles::create(['name' => 'agent', 'rus_name' => 'Агент']);
		Roles::create(['name' => 'rop', 'rus_name' => 'РОП']);
		Roles::create(['name' => 'secretary', 'rus_name' => 'Секретарь']);
		Roles::create(['name' => 'lawyer', 'rus_name' => 'Юрист']);
		Roles::create(['name' => 'ois_u', 'rus_name' => 'ОИС-Ю']);
		Roles::create(['name' => 'ois_s', 'rus_name' => 'ОИС-Ш']);
		Roles::create(['name' => 'ovs', 'rus_name' => 'ОВС']);
	}
}